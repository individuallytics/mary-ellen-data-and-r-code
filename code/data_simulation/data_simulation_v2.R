rm(list = ls())
library(ggplot2)
library(dplyr)
library(shiny)
require(cowplot)

set.seed(123)
## Setting up the working directory
setwd("C:/Users/Akash Rastogi/Desktop/UMich_study_material/Summer_2017/Internship")
data <- readRDS("./data_files/appData06262017.RDS")


# Adding the age variable
age = data.frame(subjectID=1:5000, age=runif(5000, 25, 35))
data_v2 <- left_join(data, age)
data_v2$age <- as.integer(data_v2$age)

#saveRDS(data_v2, './data_files/appData08112017.RDS')
# Dividing depression into 3 main categories
sd(data_v2$depression)
quantile(data_v2$depression, probs=c(0, 0.1, 0.25, 0.5, 0.75, 0.9, .99, 1))
# 0%       10%       25%       50%       75%       90%       99%      100% 
#   4.198429 14.812349 17.979240 21.668189 24.237117 26.775574 32.058276 40.533033


# # input id 
# sel_sub <- 30
# cohortSpecs <- data_v2 %>%
#   filter(subjectID == sel_sub)
# 
# cohortSubjectID <- data_v2 %>%
#   filter(day == 90,
#          age == cohortSpecs$age[90],
#          depression <= cohortSpecs$depression[90] - sd(data_v2$depression),
#          gender == cohortSpecs$gender[90],
#          region == cohortSpecs$region[90]) %>%
#   select(subjectID)
# 
# cohortValues <- data_v2 %>%
#   filter(subjectID %in% cohortSubjectID$subjectID) %>%
#   group_by(day) %>%
#   summarise(timesStuckToDietLast7Days = mean(timesStuckToDietLast7Days),
#             timesTakenMedicationLast7Days = mean(timesTakenMedicationLast7Days),
#             timesExercisedLast7Days = mean(timesExercisedLast7Days),
#             group='cohort')
# 
# subjectValues <- cohortSpecs %>%
#   select(day, 
#          timesStuckToDietLast7Days, 
#          timesTakenMedicationLast7Days,
#          timesExercisedLast7Days) %>%
#   mutate(group='subject')
# 
# dataForGraph = rbind(cohortValues, subjectValues)
# 
# plot1 <- ggplot(data=dataForGraph, aes(x=day, y=timesStuckToDietLast7Days, group=group, 
#                               colour=group)) + labs(y = "Times stuck to diet in last 7 days", x = 'Day') +
#   geom_point() +
#   geom_line()
# 
# 
# plot2 <- ggplot(data=dataForGraph, aes(x=day, y=timesTakenMedicationLast7Days, group=group, 
#                               colour=group)) + labs(y = "Times taken medication in last 7 days", x = 'Day') +
#   geom_point() +
#   geom_line()
# 
# 
# plot3 <- ggplot(data=dataForGraph, aes(x=day, y=timesExercisedLast7Days, group=group, 
#                               colour=group)) + labs(y = "Times exercised in last 7 days", x = 'Day') +
#   geom_point() +
#   geom_line()
# 
# plot_grid(plot1, plot2, plot3, nrow=1, align='h')